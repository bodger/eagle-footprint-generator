# Eagle footprint generator

A quick project to generate QFN footprints (based on TI docs) for Eagle using Python.

Also included is a Ruby project to do simple protoboard layouts.

Includes sample board (.brd) and footprint (.scr) output.

To use the scripts, open/create an Eagle parts library, then do Data -> execute script and choose the script.

