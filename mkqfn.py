#!/usr/bin/env python

import sys

if len(sys.argv) < 2:
    print 'usage: %s <outfile> [npins]' % sys.argv[0]
    exit()

outfile = open(sys.argv[1], 'w')

if len(sys.argv) > 2:
    npins = int(sys.argv[2])
else:
    npins = 64

centerhole = False

if npins == 64:
    paddistance = 8.1
    thermalx = 4.25
    thermaly = 4.25
    nxholes = 5
    nyholes = 5
    holepitchx = 0.8
    holepitchy = 0.8
    nxpaste = 2
    nypaste = 2
    pastegap = 0.3
    pastex = 1.8
    pastey = 1.8
    pkgx = 9.0
    pkgy = 9.0
elif npins == 48:
    paddistance = 6.1
    thermalx = 4.1
    thermaly = 4.1
    nxholes = 4
    nyholes = 4
    holepitchx = 1.0
    holepitchy = 1.0
    nxpaste = 3
    nypaste = 3
    pastegap = 0.3
    pastex = 1.15
    pastey = 1.15
    pkgx = 7.0
    pkgy = 7.0
elif npins == 16:
    paddistance = 2.1
    thermalx = 1.7
    thermaly = 1.7
    nxholes = 2
    nyholes = 2
    holepitchx = 1.0
    holepitchy = 1.0
    nxpaste = 2
    nypaste = 2
    pastegap = 0.2
    pastex = 0.7
    pastey = 0.7
    pkgx = 3.0
    pkgy = 3.0
    centerhole = True

mil = 25.4
padpitch = 0.5
pasteshrink = 0.05
maskexpand = 0.07
padwidth = 0.28
padheight = 0.85
holesize = 0.3
pin1x = -3.5
pin1y =  3.5
pin1radius = 0.214
roundness = 100
docuwidth = 0.005 * mil
placewidth = 0.008 * mil
labelsize = 0.05 * mil
padradius = padwidth / 2
sidepins = npins / 4
pasteradius = padradius - pasteshrink
maskwidth = padwidth + 2 * maskexpand
masklength = padheight + maskexpand - padradius
padlength = padheight + padradius
padoffset = (paddistance + padlength) / 2
padcenter = paddistance / 2 + padradius
hrectlen = (padheight - padradius) / 2
pastelen = hrectlen
start = padpitch * (sidepins - 1) / 2

trans = [
    { 'rot':   0, 'ox': -1, 'oy':  0, 'sx':  0, 'sy':  1 },
    { 'rot':  90, 'ox':  0, 'oy': -1, 'sx': -1, 'sy':  0 },
    { 'rot': 180, 'ox':  1, 'oy':  0, 'sx':  0, 'sy': -1 },
    { 'rot': 270, 'ox':  0, 'oy':  1, 'sx':  1, 'sy':  0 },
]


def translate(x, y, tran):
    return (x * tran['ox'] + y * tran['sx'],
            x * tran['oy'] + y * tran['sy'])


def putpad(pin, padoffset, step, tran):
    x, y = translate(padoffset, step, tran)
    outfile.write("Layer Top;\n")
    outfile.write("Smd '%d' %.2f %.2f -%d R%d NOCREAM NOSTOP (%.3f %.3f);\n" %
        (pin, padlength, padwidth, roundness, tran['rot'], x, y))


def putpaste(padoffset, step, tran):
    offset = padoffset - hrectlen
    cx, cy = translate(offset, step, tran)
    x, y   = translate(padoffset, step, tran)
    xd, yd = translate(pastelen, pasteradius, tran)
    outfile.write("Layer tCream;\n")
    outfile.write("Circle 0 (%.3f %.3f) (%.3f %.3f);\n" %
        (cx, cy, cx + pasteradius, cy))
    outfile.write("Rect R0 (%.3f %.3f) (%.3f %.3f);\n" %
        (x - xd, y - yd, x + xd, y + yd))


def putmask(padoffset, step, tran):
    offset = padoffset - hrectlen
    ix, iy = translate(padoffset - hrectlen, step, tran)
    ox, oy = translate(padoffset + hrectlen, step, tran)
    outfile.write("Layer tStop;\n")
    outfile.write("Wire %f (%f %f) (%f %f);\n" %
        (maskwidth, ix, iy, ox, oy))


def puthole(pad, x, y):
    outfile.write("Change Drill %.1f;\n" % holesize);
    outfile.write("Pad 'TP$%d' octagon 0 R0 NOSTOP (%.1f %.1f);\n" %
        (pad, x, y))


outfile.write("Set Wire_Bend 2;\n")
outfile.write("Grid mm;\n");

outfile.write("Edit 'QFN%dT.pac';\n" % npins)

outfile.write("Description '%d pin quad flatpack no-lead with thermal pad';\n"
    % npins)

pin = 1

for tran in trans:
    for sidepin in range(0, sidepins):
        step = start - sidepin * padpitch
        putpad(pin, padoffset, step, tran)
        putpaste(padoffset, step, tran)
        putmask(padoffset, step, tran)
        pin += 1

outfile.write("Layer Top;\n");
outfile.write("Smd 'TP' %.2f %.2f -0 R0 NOTHERMAL NOCREAM NOSTOP (0 0);\n" %
    (thermalx, thermaly))

thermalmaskx = thermalx / 2 + maskexpand
thermalmasky = thermaly / 2 + maskexpand

outfile.write("Layer tStop;\n");
outfile.write("Rect R0 (%.3f %.3f) (%.3f %.3f);\n" %
    (-thermalmaskx, -thermalmasky, thermalmaskx, thermalmasky))

x = ((nxholes - 1) * holepitchx) / 2
y = ((nyholes - 1) * holepitchy) / 2

pad = 1

for yhole in range(0, nyholes):
    for xhole in range(0, nxholes):
        #outfile.write("Hole %.1f (%.1f %.1f);\n" %
        #    (holesize, x - (xhole * holepitchx), y - (yhole * holepitchy)))
        puthole(pad, x - (xhole * holepitchx), y - (yhole * holepitchy))
        pad += 1

if centerhole:
    puthole(pad, 0, 0)

outfile.write("Layer tCream;\n");

xstart = (nxpaste * pastex + (nxpaste - 1) * pastegap) / -2
ystart = (nypaste * pastey + (nypaste - 1) * pastegap) / -2

for ypaste in range(0, nypaste):
    for xpaste in range(0, nxpaste):
        lx = xstart + xpaste * (pastegap + pastex)
        by = ystart + ypaste * (pastegap + pastey)
        outfile.write("Rect R0 (%.3f %.3f) (%.3f %.3f);\n" %
            (lx, by, lx + pastex, by + pastey))

hpkgx = pkgx / 2
hpkgy = pkgy / 2

outfile.write("Layer tDocu;\n")

outfile.write("Wire  %.4f " % docuwidth)
outfile.write("(-%.1f  %.1f) " % (hpkgx, hpkgy))
outfile.write("(-%.1f -%.1f) " % (hpkgx, hpkgy))
outfile.write("( %.1f -%.1f) " % (hpkgx, hpkgy))
outfile.write("( %.1f  %.1f)\n" % (hpkgx, hpkgy))
outfile.write("      (-%.1f %.1f);\n" % (hpkgx, hpkgy))

outfile.write("Layer tPlace;\n")

placeoffset = start + (maskwidth + placewidth) / 2

for xs in (-1, 1):
    for ys in (-1, 1):
        x  = hpkgx * xs
        y  = hpkgy * ys
        mx = placeoffset * xs
        my = placeoffset * ys
        if xs == -1 and ys == 1:
            outfile.write("\n")
            outfile.write("Wire %.4f " % placewidth)
            outfile.write("(%.3f %.3f) " % (mx, y))
            outfile.write("(%.3f %.3f);\n" % (x, my))
            outfile.write("\n")
        else:
            outfile.write("Wire %.4f " % placewidth)
            outfile.write("(%.3f %.3f) "   % (x, y))
            outfile.write("(%.3f %.3f);\n" % (mx, y))
            outfile.write("Wire %.4f " % placewidth)
            outfile.write("(%.3f %.3f) "   % (x, y))
            outfile.write("(%.3f %.3f);\n" % (x, my))

outfile.write("Circle %.4f " % placewidth)
outfile.write("(%.3f %.3f) " % (pin1x, pin1y))
outfile.write("(%.3f %.3f);\n" % (pin1x + pin1radius, pin1y))

outfile.write("Wire %.4f " % placewidth)
outfile.write("(%.3f %.3f) " % (-hpkgx, hpkgy))
outfile.write("(%.3f %.3f) " % (-hpkgx - 0.4, hpkgy + 0.2))
outfile.write("(%.3f %.3f) " % (-hpkgx - 0.2, hpkgy + 0.4))
outfile.write("(%.3f %.3f);\n" % (-hpkgx, hpkgy))

outfile.write("Layer tNames;\n")
outfile.write("Change size %.2f;\n" % labelsize)
outfile.write("Change Align bottom left;\n")
outfile.write("Text '>NAME' R0 (-2.75 5.25);\n")

outfile.write("Layer tValues;\n")
outfile.write("Change size %.2f;\n" % labelsize)
outfile.write("Change Align bottom left;\n")
outfile.write("Text '>VALUE' R0 (-3.25 -6.5);\n")
